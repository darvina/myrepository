<?php

function getSortUrl($name){
    if (!empty($_GET['sort'][$name])) {
        $getParam = "[{$name}]=".($_GET['sort'][$name] == 1 ? -1 : 1);
    } else {
        $getParam = "[{$name}]=1";
    }
    return $getParam;
}
?>
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Фото</th>
                <th><a href="index.php?sort<?= getSortUrl('name') ?>">Название</a></th>
                <th><a href="index.php?sort<?= getSortUrl('created_at') ?>">Дата добавления</a></th>
                <th><a href="index.php?sort<?= getSortUrl('created_by') ?>">Имя добавившего товар</a></th>
                <th><a href="index.php?sort<?= getSortUrl('count') ?>">Количество отзывов</a></th>
            </tr>
        </thead>
        <tbody>
        <?php
        $productSelection = getProducts();

        foreach ($productSelection as $item) {?>
                <tr>
                    <td><img src="<?php echo $item['image']?>" class="thumbnail" height="120"></td>
                    <td><a href="index.php?view=reviews&id=<?= $item['id'] ?>"><?=$item['name'] ?></a></td>
                    <td><?=date("Y-m-d H:i:s",$item['created_at']) ?></td>
                    <td><?=$item['created_by'] ?></td>
                    <td><span class="badge"><?= $item['count'] ?></span></td>
                </tr>
        <?php }?>
        </tbody>
    </table>

<a href="index.php?view=adding"><span class="glyphicon glyphicon-plus"></span> Добавить товар</a>

