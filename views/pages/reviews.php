    <?php
    $getId = $_GET['id'];
    $productSelect = getProduct($getId);
    $reviewSelect = getReviews($getId);

    function getAverageRating($reviewSelect)
    {
        if (array_column($reviewSelect,'rating')) {
            return array_sum(array_column($reviewSelect,'rating'))/count($reviewSelect);
        } else {return 'Нет оценок';}
    }
    ?>

    <div class="col-sm-6 col-sm-offset-3">
        <div class="thumbnail">
           <img src="<?php echo $productSelect['image']?>" height="200">
           <div class="caption text-center">
               <h2><?php echo $productSelect['name']?></h2>
                <p><?php echo getAverageRating($reviewSelect) ?></p>
           </div>
        </div>
    </div>

    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>Имя оставившего отзыв</th>
            <th>Комментарий</th>
            <th>Оценка и дата добавления</th>
        </tr>
        </thead>
        <tbody>
            <?php foreach($reviewSelect as $review) : ?>
                <tr>
                    <td><?= $review['name'] ?></td>
                    <td><?= $review['comment'] ?></td>
                    <td><?= $review['rating'].' <span class="glyphicon glyphicon-star"></span> '.date('Y-m-d H:i:s',$review['created_at']) ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <form role="form" name="form" method="post">
        <div class="form-group">
            <label>Имя</label>
            <input class="form-control" name="name" placeholder="Введите своё имя">
        </div>
        <div class="form-group">
            <label>Комментарий</label>
            <textarea class="form-control" name="comment"></textarea>
        </div>
        <div class="form-group">
            <label>Оценка</label>
            <input type="number" max="10" min="1" class="form-control" name="rating" placeholder="Введите оценку">
        </div>
        <button type="submit" class="btn btn-success">Добавить</button>
    </form>

    <?php
   if (!empty($_POST)) {
       $plusTime = 60*60;

       $fieldsReview = [
           'name',
           'rating',
           'comment',
           'created_at',
           'product_id'
       ];

       $fieldsReview['name'] = $_POST['name'];
       $fieldsReview['comment'] =  $_POST['comment'];
       $fieldsReview['rating'] =  $_POST['rating'];
       $fieldsReview['created_at'] =  time() + $plusTime;
       $fieldsReview['product_id'] = $getId;

       addToReview($fieldsReview);
   }
    ?>





