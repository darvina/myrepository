
    <form role="form" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label>Название товара</label>
            <input class="form-control" name="name" placeholder="Введите название товара">
        </div>
        <div class="form-group">
            <label>Изображение товара</label>

            <input data-type="url-image" class="form-control" name="image" placeholder="Вставьте ссылку на изображение">

            <input data-type="upload-image" type="file" id="addimg" name="avatar">
        </div>
        <div class="form-group">
            <label>Средняя цена товара</label>
            <input class="form-control" name="average_price" placeholder="Введите среднюю цену товара">
        </div>
    <!--    дата добавления отдельно-->
        <div class="form-group">
            <label>Имя добавляющего товар</label>
            <input class="form-control" name="created_by" placeholder="Введите своё имя">
        </div>
        <button type="submit" class="btn btn-success">Добавить</button>
    </form>

    <script>
        $(document).ready(function(){
            $('input[data-type=url-image]').on('keyup',function(){
                var length = $(this).val().length;
                if (length > 0) {
                    $('input[data-type=upload-image]').hide();
                } else {
                    $('input[data-type=upload-image]').show();
                }
            })
            $('input[data-type=upload-image]').on('change',function(){
                var length = $(this).val().length;
                if (length > 0) {
                    $('input[data-type=url-image]').hide();
                } else {
                    $('input[data-type=url-image]').show();
                }
            })
        })
    </script>
    <?php
    if (!empty($_POST)) {
        $plusTime = 60*60;

        $fields = [
            'name',
            'image',
            'average_price',
            'created_at',
            'created_by'
        ];

        if (!empty($_FILES['avatar']) && empty($_POST['image'])) {
            $destDir = PROJECT_ROOT.'/uploads/';
            if (!is_dir($destDir)) {
                mkdir($destDir,0777);
            }
            move_uploaded_file($_FILES['avatar']['tmp_name'],$destDir.$_FILES['avatar']['name']);
            $fields['image'] =  'uploads/'.$_FILES['avatar']['name'];

        } else {
            $fields['image'] =  $_POST['image'];
        }
        $fields['name'] = $_POST['name'];
        $fields['created_at'] =  time() + $plusTime;
        $fields['created_by'] = $_POST['created_by'];
        $fields['average_price'] = $_POST['average_price'];

        addToProduct($fields);

        header('Location: index.php');
    }

    ?>


