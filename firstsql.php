<?php

    function dbConnect()
    {
        $host = 'localhost';
        $user = 'root';
        $db = 'products';


        $connection = mysql_connect($host, $user);
        if (!$connection || !mysql_select_db($db, $connection)) {
            return false;
        }
        mysql_query("SET NAMES utf8 COLLATE utf8_unicode_ci");
        return $connection;
    }


    function convertToArray($result,$one = false)
    {
        if ($one === false) {
            $resArray = [];
            while($row = mysql_fetch_assoc($result)) {
                $resArray[] = $row;
            }

            return $resArray;
        }
        return mysql_fetch_assoc($result);

    }

    function getSortType($str){
        if ($str == -1 ){
            return 'DESC';
        } else {
            return 'ASC';
        }
    }

    function getProduct($id){
        dbConnect();
        $query = "SELECT * FROM product WHERE id = '{$id}'";
        $result = mysql_query($query);
        $result = convertToArray($result,true);
        return $result;
    }

    function getProducts()
    {
        dbConnect();
        $fields = [
            'name',
            'created_at',
            'created_by',
            'count'
        ];
        $query = "SELECT product.*, count(review.id) as count FROM product LEFT JOIN review ON product.id = review.product_id GROUP BY product.id";
        if (!empty($_GET['sort']) && in_array(($fieldName = array_shift(array_keys($_GET['sort']))),$fields)) {
            $query = "SELECT product.*, count(review.id) as count FROM product LEFT JOIN review ON product.id = review.product_id GROUP BY product.id ORDER BY {$fieldName} ".getSortType($_GET['sort'][$fieldName]);

        }
        $result = mysql_query($query);
        $result = convertToArray($result);
        return $result;

    }

    function getReviews($id)
    {
        dbConnect();

        $query = "SELECT * FROM review WHERE product_id = '{$id}'";
        $result = mysql_query($query);
        $result = convertToArray($result);
        return $result;
    }

    function addToProduct($fields)
    {
        dbConnect();

        $query = "INSERT INTO product(name, image, created_at, created_by, average_price)";
        $query = $query . " VALUES ('{$fields['name']}', '{$fields['image']}', {$fields['created_at']}, '{$fields['created_by']}', {$fields['average_price']})";
        mysql_query($query) or die(mysql_error());
        mysql_close();
    }

    function addToReview($fieldsReview)
    {
        dbConnect();

        $query = "INSERT INTO review(name, rating, comment, created_at, product_id)";
        $query = $query . " VALUES ('{$fieldsReview['name']}', {$fieldsReview['rating']}, '{$fieldsReview['comment']}', {$fieldsReview['created_at']}, {$fieldsReview['product_id']})";
        mysql_query($query) or die(mysql_error());
        mysql_close();
    }

